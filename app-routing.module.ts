import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductListComponent } from "./product-list/product-list.component";
import { ProductDetailComponent } from './product-detail/product-detail.component';
import {LoginComponent} from './login/login.component';
import {AdminComponent} from './admin/admin.component';
import {AuthGuard} from './auth.guard';

const routes: Routes = [{path: 'products' , component: ProductListComponent},
  {path: 'product/:id' , component: ProductDetailComponent},
  {path: '',  redirectTo: '/products', pathMatch: 'full' },
  {path: 'login', component: LoginComponent},
  {path: 'admin', component: AdminComponent, canActivate: [AuthGuard]}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
